# oftik-parent

Provides parent pom.xml for various downstream projects. I can't remember all the tweaks, so I wrote them down. Maven runs without all the fancy stuff here, that's for sure. But sane source building, checking and releasing requires effort to be enabled explicitely.

Some features even require deeper knowledge of the Maven build system and how it interacts with the/ your favorite JDK.

# Hints and Notes

## java.version

Use toolchains and set java.version per project/ parent POM. Compiler plugin is still provided but set source and target accordingly.

## OWASP

This is bound to a profile to be enabled on demand. It delays builds if run for the first time. It loads the entire CVE-database to the host. If this is a virtual or container environment that is destroyed or wiped afterwards think about volumes from outside or caching across instances.

Since the datbase is only growing the delay also increases over the years. Small hint: write less errors and introduce less vulnerabilities. Maybe by enabling various checks and follow policies requiring code reviews and audits.

## jdeps

This tool is broken until JDK 14 in combination with Maven. See the issues linked in the pom.xml for details and more recent updates. Basically the mixture of non-Jigsaw libraries with those already providing (sane) module definitions breaks multi-module setups. In turn `jdeps` refuses to work and exits even on the command line. This is not a Maven issue but a matter of the tool and its class- and module path interpretation. Use with caution and only with JDK 14 and later.

## Release

* commit all local changes, incl. Change log
* `mvn release:prepare` – creates Git-tag
* `mvn release:perform` – does a clean install based on the tag
* `mvn release:clean` – cleans temporary files necessary for rollback

## Reporting

Since it cannot be managed by the parent project specifies necessary reports explicitely.

```
<project>
    <!-- ... -->
    
    <!-- Since it cannot be managed by the parent project specifies necessary reports explicitely. -->
    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </reporting>
    
</project>
```

## Eclipse Lifecycle Mapping

Eclipse does not evaluate nor integrate Maven natively. Instead the so called m2e plugin (Maven 2 Eclipse) merges Maven POMs into Eclipse runtime.

The m2e plugin does not understand all tweaks a POM could take. Therefore lifecycle mappings were introduced to Eclipse. There are two basic choices with lifecycle mappings: configure m2e from the POM or have an external configuration file.

I prefer the latter an external configuration to keep POMs clean. But to still have it reproducible and common for all my projects a XML configuration file `lifecycle-mapping-metadata.xml` is part of this project.

Preferrably copy it to have control over possible breaking changes. I personally use the .m2 directory.

Finally configure Eclipse' Maven section Lifecyle Mappings to use this external file.

# History

see [Changelog](CHANGELOG.md)

