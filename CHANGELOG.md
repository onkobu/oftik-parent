# 1.0.11

- Disabled antrun plugin for seucrity reasons
- version bumps

# 1.0.10

- improve scm properties, connection and url

# 1.0.9

- PMD with executes/ report goal
- Changelog as separate file
- Eclipse Lifecycle Mappings
- Version bumps, SQLite

# 1.0.8

- Version bumps

# 1.0.7

* java.version dropped, switch to toolchains/ invididual settings

# 1.0.2

* JaCoCo – Code coverage
* OWASP dependency check – query CVE-database with dependencies for vulnerabilities, `-Powasp-dependency-check`

# 1.0.1

* PMD – Static code analysis

# 1.0

* Sourceforge, release through maven
* SQLite – JDBC driver
* Maven enforcer – sane Maven runtime
